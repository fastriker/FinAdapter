lower_plate_length = 185;
lower_plate_small_width = 30;
lower_plate_big_width = 34;
lower_plate_height = 4;

lower_upper_distance = 4.75;

upper_plate_length = lower_plate_length-lower_upper_distance;
upper_plate_small_width = 16.5;
upper_plate_big_width = 19.4;
upper_plate_height = 6;
upper_plate_height2 = 2*upper_plate_height;

pseudo_fin_length = 0.9*upper_plate_length; 
pseudo_fin_width = 5;
pseudo_fin_height = 2;

hole_length = 16;
hole_width = 4.5;
hole_bottom_height = 5.5;
hole_distance = 36+5;

screw_diameter = 4.5;
screw_head_diameter = 11;
screw_slope_height = 5;
screw_head_hole_height = 2;
screw_length = 20;

screw_distance1 = 23;
screw_distance2 = 25;
screws = 6;

angle = 40;

tolerance = 0.2;
epsilon = 0.001;
layer_height = 0.2;

$fn = 80;

screw_holes()
{
    adapter(with_slope=true)
    {
//        import_fin();
        adapter_fin();
//        pseudo_fin();
    }
}

module slope_cutoff()
{
    height = lower_plate_height + upper_plate_height;
    tmp = height * tan(90-angle);
    
    translate([-lower_upper_distance+lower_plate_length/2-tmp, 0 , 0])
    {
        children();
    }
}

module adapter_fin()
{
    size = lower_plate_big_width;
    height = lower_plate_height + upper_plate_height;
    tmp = height * tan(90-angle);
    
    difference()
    {
        upper_plate();
        slope_cutoff()
        {
            translate([0, -size/2, 0])
            {
                cube([size, size, size]);
            }
        }
    }
}

module import_fin()
{
        translate([80, 0, -5])
    {
        rotate([90, 0, 180])
        {
            import("untitled.stl", convexity=3);
        }
    }
}

module pseudo_fin()
{
    size = lower_plate_big_width;
    
    difference()
    {
        hull()
        {
            for(i = [-1,1])
            {
                translate([i*(pseudo_fin_length/2-pseudo_fin_height/2), 0, 0])
                {
                    rotate([90, 0, 0])
                    {
                        cylinder(r=pseudo_fin_height, h=pseudo_fin_width, center=true);
                    }
                }
            }
        }
        translate([0, 0, -pseudo_fin_height/2-epsilon])
        {
            cube([pseudo_fin_length+2*pseudo_fin_height, pseudo_fin_width+2*epsilon, pseudo_fin_height+2*epsilon], center=true);
        }
        slope_cutoff()
        {
            rotate([0, -90+angle, 0])
            {
                translate([0, -size/2, -size/2])
                {
                    cube([size, size, size]);
                }
            }
        }
    }
}

//!pseudo_fin();

module adapter(with_slope=true, rec=false)
{
    difference()
    {
        union()
        {
            difference()
            {
                tmp = lower_plate_height+upper_plate_height;
                union()
                {
                    lower_plate();
                    translate([lower_upper_distance, 0, lower_plate_height])
                    {
                        upper_plate();
                    }
                }
                if(with_slope)
                {
                    translate([lower_plate_length/2, 0, 0])
                    {
                        rotate([0, angle, 0])
                        {
                            translate([-2*tmp, -lower_plate_big_width/2, 0])
                            {
                                cube([3*tmp, lower_plate_big_width, tmp]);
                            }
                        }
                    }
                }
            }
            translate([lower_upper_distance, 0, lower_plate_height+upper_plate_height])
            {
                children();
            }
        }        
        translate([-lower_plate_length/2+hole_distance, -lower_plate_big_width/2, hole_bottom_height])
            {
                cube([hole_length+2*tolerance, lower_plate_big_width+2*tolerance, hole_width]);
            }
        }
}


module lower_plate()
{
    tmp = lower_plate_height+upper_plate_height;
    difference()
    {
        hull()
        {
            translate([-lower_plate_length/2, 0, lower_plate_height/2])
            {
                cube([epsilon, lower_plate_small_width, lower_plate_height], center=true);
            }
            translate([lower_plate_length/2, 0, lower_plate_height/2])
            {
                cube([epsilon, lower_plate_big_width, lower_plate_height], center=true);
            }
        }
        translate([-lower_plate_length/2, 0, 2])
        {
            rotate([0, angle, 180])
            {
                translate([-tmp, -lower_plate_big_width/2, 0])
                {
                    cube([2*tmp, lower_plate_big_width, tmp]);
                }
            }
        }
    }
}

module upper_plate()
{
    hull()
    {
        translate([-lower_plate_length/2+upper_plate_small_width/2, 0, upper_plate_height/2])
        {
            cylinder(d=upper_plate_small_width-2*tolerance, h=upper_plate_height, center=true);
        }
        translate([lower_plate_length/2-lower_upper_distance, 0, upper_plate_height/2])
        {
            cube([epsilon, upper_plate_big_width-2*tolerance, upper_plate_height], center=true);
        }
    }
}

module screw(print_layer=false)
{
    difference()
    {
        union()
        {
            cylinder(d=screw_head_diameter+2*tolerance, h=screw_head_hole_height);
            translate([0, 0, screw_head_hole_height])
            {
                cylinder(d1=screw_head_diameter+2*tolerance, d2=screw_diameter+2*tolerance, h=screw_slope_height);
                cylinder(d=screw_diameter+2*tolerance, h=screw_length);
            }
            
        }
        if(print_layer)
        {
            translate([0, 0, screw_head_hole_height+screw_slope_height])
            {
                cylinder(d=screw_diameter+2*tolerance, h=layer_height);
            }
        }
    }
}

//!screw(false);

module screw_holes()
{
    difference()
    {
        children();
        translate([lower_plate_length/2, 0, 0])
        {
            translate([-screw_distance1, 0, 0])
            {
                for(i = [0:screws]){
                    translate([-screw_distance2*i, 0, 0])
                {
                    screw();
                    
                }
            }
            }
        }
    }
}